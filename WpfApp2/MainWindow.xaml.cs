﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp2.parse;

namespace WpfApp2
{
   
    public partial class MainWindow : Window
    {
        private String func;
        private Parse v;
        Window2 k;
        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindow(int x)
        {
            InitializeComponent();
            InitializeComponent();
            textBox.Text = x.ToString();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            //func = textBox.Text;
            //v = new Parse(func);
            k = new Window2(int.Parse(textBox.Text));
            k.Show();
            this.Close();
        }
    }
}
