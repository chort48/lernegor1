﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WpfApp2.parse
{
    class Parse
    {
        private int b;
        private Stack<Znack> function;
        private double[] func;
        public Parse(String function)
        {
            func = new double[function.Length];
            this.function = new Stack<Znack>();
            for(int i = 0; i < function.Length; ++i)
            {
                if(function[i] == '+')
                {
                    this.function.Push(new Znack(Double.Parse(function[i - 2].ToString()), Double.Parse(function[i + 2].ToString()), "+"));
                }
                if(function[i] == '-')
                {
                    this.function.Push(new Znack(Double.Parse(function[i - 2].ToString()), Double.Parse(function[i + 2].ToString()), "-"));
                }
                if(function[i] == '*')
                {
                    this.function.Push(new Znack(Double.Parse(function[i - 2].ToString()), Double.Parse(function[i + 2].ToString()), "*"));
                }
                if(function[i] == '/')
                {
                    this.function.Push(new Znack(Double.Parse(function[i - 2].ToString()), Double.Parse(function[i + 2].ToString()), "/"));
                }
            }

        }


        public int getParse()
        {
            return b;
        }
    }
}
