﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp2.parse
{
    class Znack
    {
        private double left;
        private double right;
        private string znack;

        public Znack(double left, double right, string znack)
        {
            this.left = left;
            this.right = right;
            this.znack = znack;
        }

        double answer()
        {
            try
            {
                if (znack == "+")
                    return left + right;
                if (znack == "-")
                    return left - right;
                if (znack == "*")
                    return left * right;
                if (znack == "/")
                    return left / right;
                else
                    return 0;
            }
            catch
            {
                return 0;
            }
        }
    }
}
